# Omer Count

A small Common Lisp script to aid in the counting of the days and
weeks from Pesach to Shavuot.

A discussion of the purpose and requirements of this script can be
found at [this blog
article](http://www.eclecticse.com.au/2016/10/lisp-to-ada-omer-count-part-one.html),
but here is the essential summary.

## What is an omer count and what is the purpose of this script?

There are two biblical feasts called Pesach and Shavuot, where the
latter is fifty days after the former. There is a biblical commandment
to count the days, the weeks and the weekly Sabbaths which occur
between 16 Aviv (the day after Pesach) and the day of Shavuot, fifty
days later.

The Bible gives a few instructions on the counting requirements:

1. The counting is to begin (from one) on the day after the Pesach
   Sabbath on 15 Aviv.
2. The days from one to forty-nine are to be counted, and then the
   fiftieth day is to be celebrated as the day of Shavuot.
3. The celebration of the fiftieth day is to begin at sunset of the
   previous day.
4. The seven weekly Sabbath days along the way are also to be
   separately counted.
5. Each of the seven weeks is to be counted.

We take it that to do this counting, the intention is that it be done
each day until the eve of Shavuot arrives; and therein lies the
problem which this program is meant to solve: a single person
performing this count manually each day may possibly be error prone.
It's handy to have a convenient confirmation that one has not skipped
or doubled up on a day.

## Usage

```lisp
(ql:quickload :eclecticse.omer)
(use-package :eclecticse.omer)
(let ((omer:*pesach* "2019-03-23"))
  (omer)
  (omer-for "2019-05-12"))
```
