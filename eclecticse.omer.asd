;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(asdf:defsystem "eclecticse.omer"
  :version "1.0.1"
  :author "David Trudgett <eclecticse@gmail.com>"
  :licence "GPL V3"
  :description "A script to assist in counting the time period between Pesach and Shavuot."
  :serial t
  :components ((:file "omer-package")
               (:file "omer"))
  :depends-on ("local-time"))
